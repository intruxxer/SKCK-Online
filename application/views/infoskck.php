<div class="row">
<div class="col-xs-12" id="aboutlang">
<h3 style="text-align:center"><strong>TENTANG SKCK ONLINE</strong></h3>

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Dokumen SKCK</a></li>
    <li role="presentation"><a href="#cara" aria-controls="profile" role="tab" data-toggle="tab">Tata Cara</a></li>
    <li role="presentation"><a href="#syarat" aria-controls="messages" role="tab" data-toggle="tab">Persyaratan Aplikasi</a></li>
    <li role="presentation"><a href="#ketentuan" aria-controls="settings" role="tab" data-toggle="tab">Ketentuan Pengambilan</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
    	<p style="text-align:justify"></p>
    	<blockquote>
		<p style="text-align:justify">
		  Surat Keterangan Catatan Kepolisian atau SKCK adalah surat keterangan resmi yang diterbitkan oleh POLRI 
		  melalui fungsi Intelkam kepada seseorang pemohon/warga masyarakat untuk memenuhi permohonan dari yang bersangkutan atau suatu keperluan 
		  karena adanya ketentuan yang mempersyaratkan, berdasarkan hasil penelitian biodata dan catatan Kepolisian yang ada tentang orang tersebut 
		  (Vide Peraturan Kapolri Nomor 18 Tahun 2014). SKCK memiliki masa berlaku sampai dengan 6 (enam) bulan sejak tanggal diterbitkan. 
		  Jika telah melewati masa berlaku dan bila dirasa perlu, SKCK dapat diperpanjang oleh yang bersangkutan.
    	</p>
    	</blockquote>
    </div>
    <div role="tabpanel" class="tab-pane" id="cara">
    	<p style="text-align:justify"></p>
    	<p style="text-align:justify"><strong>Tata cara permohonan untuk memperoleh SKCK dapat dilakukan dengan cara :</strong></p>
	    <blockquote>
	    	<ul>
				<li style="text-align:justify">manual/mendaftar secara langsung di loket pelayanan SKCK yang berada di kantor Polsek/Polres/Polda/Markas Besar dengan membawa dokumen yang dipersyaratkan serta mengisi formulir yang telah disiapkan oleh petugas; atau</li>
				<li style="text-align:justify">melakukan pendaftaran permohonan SKCK secara <em>online</em> dengan cara mengisi form yang tersedia sesuai dengan urutan.</li>
			</ul>
		</blockquote>
    </div>
    <div role="tabpanel" class="tab-pane" id="syarat">
    	<p style="text-align:justify"></p>
	    <p style="text-align:justify"><strong>Dokumen yang dipersyaratkan untuk permohonan SKCK secara <em>online</em> adalah sebagai berikut :</strong></p>
		<p style="text-align:justify">1. Pemohon Warga Negara Indonesia (WNI):</p>
		<blockquote>
			<ul>
				<li style="text-align:justify"><em>Copy Scan</em> KTP asli;</li>
				<li style="text-align:justify"><em>Copy Scan</em> Kartu Keluarga (KK) asli;</li>
				<li style="text-align:justify"><em>Copy Scan</em> Akte Kenal Lahir asli;</li>
				<li style="text-align:justify"><em>Copy Scan</em> identitas lain bagi pemohon yang belum memenuhi syarat memperoleh KTP;</li>
				<li style="text-align:justify"><em>Copy Scan</em> foto diri ukuran 4 x 6 berwarna latar belakang merah, berpakaian sopan, tampak muka. Bagi pemohon yang mengenakan jilbab harus tampak muka;</li>
				<li style="text-align:justify"><em>Copy Scan</em> Paspor bagi WNI yang akan keluar negeri dalam rangka sekolah/kunjungan/penerbitan VISA;</li>
			</ul>
		</blockquote>

		
		<p style="text-align:justify">2. Pemohon Warga Negara Asing (WNA):</p>
		<blockquote>
			<ul>
				<li style="text-align:justify"><em>Copy Scan</em> Surat permohonan (asli) sponsor, perusahaan, lembaga yang mempekerjakan, menggunakan atau yang bertanggung jawab terhadap WNA;</li>
				<li style="text-align:justify"><em>Copy Scan</em> Paspor asli;</li>
				<li style="text-align:justify"><em>Copy Scan</em> Kartu Izin Tinggal Terbatas (KITAS) asli atau Kartu Izin Tinggal Tetap (KITAP) asli;</li>
				<li style="text-align:justify"><em>Copy Scan</em> foto diri ukuran 4 x 6 berwarna latar belakang kuning, berpakaian sopan, tampak muka. Bagi pemohon yang mengenakan jilbab harus tampak muka;</li>
				<li style="text-align:justify"><em>Copy Scan</em> Surat Nikah asli dan KTP asli suami/istri bagi WNA yang mendapat sponsor dari WNI.</li>
			</ul>
		</blockquote>

		<p style="margin-left:20px; text-align:justify">Pada saat pengambilan SKCK di loket pelayanan, pemohon WAJIB menunjukkan dokumen asli yang dipersyaratkan di atas kepada petugas loket guna keperluan verifikasi.</p>
    </div>

    <div role="tabpanel" class="tab-pane" id="ketentuan">
    	<p style="text-align:justify"></p>
		<p style="text-align:justify"><strong>Ketentuan pengambilan SKCK :</strong></p>
		<blockquote>
			<ul>
				<li style="text-align:justify">Pemohon SKCK yang melakukan registrasi <em>online</em> sebelum pukul 08:00 waktu setempat dapat mengambil SKCK di loket pelayanan sampai dengan pukul 14:00 pada hari yang sama dengan membawa dan menunjukkan kode registrasi serta dokumen yang dipersyaratkan kepada petugas loket pelayanan;</li>
				<li style="text-align:justify">Pemohon SKCK yang telah melakukan registrasi <em>online</em> diberikan kesempatan untuk mengambil SKCK paling lama 3 (tiga) hari kerja. Bila melebihi waktu tersebut, sistem akan otomatis menghapus data pemohon dan pemohon harus melakukan registrasi ulang.</li>
			</ul>
		</blockquote>
    </div>

  </div>

</div>

</div>
</div>

<script type="text/javascript">

</script>
